import datetime

from flask import Flask, render_template, url_for
from google.cloud import bigquery

app = Flask(__name__)

@app.route("/")
def hello_world():
    return render_template('index.html')

@app.route("/demo/cursoplatzi/getpredictions")
def get_predictions():
    query_prediction = """
    SELECT
*
FROM
  ml.PREDICT(MODEL `cursogoogleplatzi.classification_model_2`,
   (
WITH all_visitor_stats AS (
SELECT
  fullvisitorid,
  IF(COUNTIF(totals.transactions > 0 AND totals.newVisits IS NULL) > 0, 1, 0) AS will_buy_on_return_visit
  FROM `data-to-insights.ecommerce.web_analytics`
  GROUP BY fullvisitorid
)
  SELECT
      CONCAT(fullvisitorid, '-',CAST(visitId AS STRING)) AS unique_session_id,
      # labels
      will_buy_on_return_visit,
      MAX(CAST(h.eCommerceAction.action_type AS INT64)) AS latest_ecommerce_progress,
      # behavior on the site
      IFNULL(totals.bounces, 0) AS bounces,
      IFNULL(totals.timeOnSite, 0) AS time_on_site,
      totals.pageviews,
      # where the visitor came from
      trafficSource.source,
      trafficSource.medium,
      channelGrouping,
      # mobile or desktop
      device.deviceCategory,
      # geographic
      IFNULL(geoNetwork.country, "") AS country
  FROM `data-to-insights.ecommerce.web_analytics`,
     UNNEST(hits) AS h
    JOIN all_visitor_stats USING(fullvisitorid)
  WHERE
    # only predict for new visits
    totals.newVisits = 1
    AND date BETWEEN '20170701' AND '20170801' # test 1 month
  GROUP BY
  unique_session_id,
  will_buy_on_return_visit,
  bounces,
  time_on_site,
  totals.pageviews,
  trafficSource.source,
  trafficSource.medium,
  channelGrouping,
  device.deviceCategory,
  country
)
)
ORDER BY
  predicted_will_buy_on_return_visit DESC
LIMIT 10;
  """

    client = bigquery.Client()

    execution_date = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")

    destination_table_id = "personaltest-284417.cursogoogleplatzi.prediccion-" + execution_date

    job_config = bigquery.QueryJobConfig(destination=destination_table_id)

    query_job = client.query(query_prediction, job_config=job_config)

    results = query_job.result()

    print("Query results loaded to the table {}".format(destination_table_id))

    """
    for row in results:
        print("unique_session_id: {}".format(row.unique_session_id))
    """
    return ""

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=8080)   
